import { Meduse } from "./entities";
import data from "./meduse.json";

const playGround: HTMLElement = document.querySelector("body")
const main = document.querySelector("main")
const layout = [
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 2, 2, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1],
    [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 2, 2, 2, 2, 2, 2, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
    [1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 2, 2, 2, 2, 2, 2, 2, 2, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1],
    [1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 1],
    [1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1],
    [1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1],
    [1, 0, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1],
    [1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1],
    [1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1],
    [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
]
const monScore = document.querySelector("h4");
let score = 0;
const gameStart: HTMLElement = document.querySelector(".start");
const gameOver: HTMLElement = document.querySelector(".gameOver");
const win: HTMLElement = document.querySelector(".win");
const buttons = document.querySelectorAll("button");
gameOver.style.display = "none";
win.style.display = "none";
let meduses: Meduse[] = data;

const fishElement: HTMLImageElement = document.querySelector(".fish");
const fish = {
    x: 14,
    y: 14,
}

updateMap()

for (const button of buttons) {
    button.addEventListener("click", () => {
        location.reload()
    })
}


playGround.addEventListener("keydown", (e) => {
    main.innerHTML = " ";
    gameStart.style.display = "none";
    let vertical = fish.x;
    let horizontal = fish.y;
    if (e.key === "ArrowUp") {
        fish.x--;
        fishElement.classList.add("bas")
        fishElement.classList.remove("haut")
    }

    if (e.key === "ArrowDown") {
        fish.x++;
        fishElement.classList.add("haut")
        fishElement.classList.remove("bas")
    }

    if (e.key === "ArrowLeft") {
        fish.y--;
        fishElement.classList.add("reverse")
        fishElement.classList.remove("bas")
        fishElement.classList.remove("haut")
    }

    if (e.key === "ArrowRight") {
        fish.y++;
        fishElement.classList.remove("reverse")
        fishElement.classList.remove("bas")
        fishElement.classList.remove("haut")
    }

    if (fish.y == -1) {
        fish.y = 27;
    }
    if (fish.y == 28) {
        fish.y = 0;
    }

    if (layout[fish.x][fish.y] == 1) {
        fish.x = vertical;
        fish.y = horizontal;
    }
    if (layout[fish.x][fish.y] == 0) {
        score += 10;
        layout[fish.x][fish.y] = 2;
        console.log(score);

    }
    moveMeduse()
    updateMap()
})


function moveMeduse() {
    for (const item of meduses) {
        let verMed = item.x;
        let horMed = item.y;
        let move = Math.random()
        
        if (move <= 0.50) {
            if (item.x < fish.x) {
                item.x++;
            } else {
                item.x--;
            }
        } else {
            if (item.y < fish.y) {

                item.y++;
            } else {
                item.y--;
            }
        }
        if (layout[item.x][item.y] == 1) {
            item.x = verMed;
            item.y = horMed;
        }
    }
}

function updateMap() {
    for (const item of meduses) {
        if (item.x === fish.x && item.y === fish.y) {
            fishElement.src = "/img/arete.gif";
            gameOver.style.display = "block";
            gameOver.style.display = "flex";
        }
    }
    
    for (let i = 0; i < layout.length; i++) {
        const row = document.createElement("div");
        row.classList.add("play");

        for (let j = 0; j < layout[i].length; j++) {
            const item = layout[i][j];
            const colo = document.createElement("div");
            colo.classList.add("tile");
            row.append(colo);
            if (item === 1) {
                colo.classList.add("wall");
            }
            if (item === 0) {
                const graine = document.createElement("img");
                graine.src = "/img/graines-de-tournesol.png";
                graine.classList.add("graine");
                colo.append(graine)
            }
            if (item === 2) {
                colo.classList.add("rien");
            }

            if (i === fish.x && j === fish.y) {
                colo.append(fishElement);
            }
            for (const item of meduses) {
                const meduse = document.createElement("img")
                meduse.src = item.src;
                meduse.classList.add("fish")
                if (i === item.x && j === item.y) {
                    colo.append(meduse)
                }
            }

        }
        if (score === 2640) {
            win.style.display = "block";
            win.style.display = "flex";
        }
        monScore.textContent = "Score : " + score;
        main.append(row);
    }

}
